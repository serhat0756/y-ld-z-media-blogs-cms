<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController');
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::middleware(['is_admin'])->group(function () {
	Route::prefix('dashboard')->group(function () {
		Route::get('/', 'DashboardController')->name('dashboard');

		// User Routes
		Route::get('/users/{user}/restore', 'UserController@restore')->name('users.restore');
		Route::get('/users/{users}/harddelete', 'UserController@harddelete')->name('users.hard.delete');
		Route::get('/users/switch', 'UserController@switch')->name('users.switch');
		Route::get('/users/archive', 'UserController@archive')->name('users.archive');
		Route::resource('/users', 'UserController');

		//Category Routes
		Route::get('/category/{category}/restore', 'CategoryController@restore')->name('category.restore');
		Route::get('/category/{category}/harddelete', 'CategoryController@harddelete')->name('category.hard.delete');
		Route::get('/category/rank', 'CategoryController@rank')->name('category.rank');
		Route::get('/category/switch', 'CategoryController@switch')->name('category.switch');
		Route::get('/category/archive', 'CategoryController@archive')->name('category.archive');
		Route::resource('/category', 'CategoryController');

        //Menu Routes
		Route::get('/menu/{menu}/restore', 'MenuController@restore')->name('menu.restore');
		Route::get('/menu/{menu}/harddelete', 'MenuController@harddelete')->name('menu.hard.delete');
		Route::get('/menu/archive', 'MenuController@archive')->name('menu.archive');
		Route::get('/menu/rank', 'MenuController@rank')->name('menu.rank');
		Route::get('/menu/switch', 'MenuController@switch')->name('menu.switch');
		Route::resource('/menu', 'MenuController');

		// Product Routes
		Route::get('/product/restore', 'ProductController@restore')->name('product.restore');
		Route::get('/product/harddelete', 'ProductController@harddelete')->name('product.harddelete');
		Route::get('/product/rank', 'ProductController@rank')->name('product.rank');
		Route::get('/product/switch', 'ProductController@switch')->name('product.switch');
		Route::get('/product/product', 'ProductController@archive')->name('product.archive');
		Route::resource('/product', 'ProductController');

	});
});


