<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Category extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'name',
		'description',
		'parent',
		'slug',
		'status',
	];
	
}
