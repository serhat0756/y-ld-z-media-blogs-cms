<?php

namespace App\Providers;

use App\Observers\UserObserver;
use App\Observers\CategoryObserver;
use App\Observers\MenuObserver;
use App\User;
use App\Category;
use App\Menu;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Category::observe(CategoryObserver::class);
        Menu::observe(MenuObserver::class);
    }
}
