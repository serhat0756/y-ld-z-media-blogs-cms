<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\UserStore;
use App\Http\Requests\UserUpdate;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'DESC')->get();

        return view('dashboard.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\UserStore $request
     *
     * @return \Illuminate\Http\Response

     */

    public function store(UserStore $request)
    {
        User::create($request->validated());

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('dashboard.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('dashboard.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UserUpdate $request
     * @param \App\User                     $user
     *
     * @return void
     */
    public function update(UserUpdate $request, User $user)
    {   

        if ($request->filled('password')) {
            $request->merge([
                'password' => $request->input('password'),
            ]);
        }
        $user->update($request->all());

        return redirect()->route('users.edit', $user->id)->withNotify('User updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index')->withNotify('User arsived.');
    }

    public function switch(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->status = $request->status=='true' ? 1 : 0;
        $user->update();
    }

    public function archive(){

        $users=User::onlyTrashed()->get();
        return view('dashboard.users.archive', compact('users'));
    }

    public function restore($id)
    {
        $user = User::onlyTrashed()->findOrFail($id);
        $user->restore();
        return redirect()->route('users.archive')->withNotify('User restored.');
    }

    public function harddelete($id)
    {
        User::onlyTrashed()->findOrFail($id)->forceDelete();
        return redirect()->route('users.archive')->withNotify('User deleted.');
    }
}
