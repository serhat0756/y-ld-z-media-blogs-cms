<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\Menu\Store;
use App\Http\Requests\Menu\Update;
use App\Menu;


class MenuController extends Controller
{

    public function __construct(){

        return view()->share('menus', Menu::orderBy('rank', 'ASC')->get());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.menus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.menus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        Menu::create($request->validated());
        return redirect()->route('menu.index')->withNotify('Menu created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        return view('dashboard.menus.show', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
       return view('dashboard.menus.edit', compact('menu'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Menu $menu)
    {
        $menu->update($request->all());
        return redirect()->route('menu.edit', $menu->id)->withNotify('Menu updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        return redirect()->route('menu.index')->withNotify('Menü archived.');
    }

    public function switch(Request $request){

        $menu = Menu::findOrFail($request->id);
        $menu->status = $request->status=='true' ? 1 : 0;
        $menu->update();
    }

    public function rank(Request $request){

     foreach ($request->get('ord') as $key => $value) {
        Menu::where(['id' => $value])->update(['rank' =>$key]);
    }
}

public function archive(){

    $menus = Menu::onlyTrashed()->orderBy('rank', 'ASC')->get();
    return view('dashboard.menus.archive',compact('menus'));
}

public function restore($id){

    $menu = Menu::onlyTrashed()->findOrFail($id);
    $menu->restore();
    return redirect()->route('menu.archive')->withNotify('Menu restored.');
}

public function harddelete($id){

    Menu::onlyTrashed()->findOrFail($id)->forceDelete();
    return redirect()->route('menu.archive')->withNotify('Menu deleted.');
}


}
