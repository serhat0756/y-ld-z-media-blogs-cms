<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Category;
use App\Http\Requests\Category\Store;
use App\Http\Requests\Category\Update;

class CategoryController extends Controller
{

    public function __construct(){

        view()->share('categories', Category::orderBy('rank', 'ASC')->get());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('dashboard.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view('dashboard.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request) {

        Category::create($request->validated());
        return redirect()->route('category.index')->withNotify('Category created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category){

        return view('dashboard.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category){

        return view('dashboard.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Category $category) {

     $category->update($request->all());
     return redirect()->route('category.edit', $category->id)->withNotify('Category updated.');
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category) {

        $category->delete();
        return redirect()->route('category.index')->withNotify('Category archived.');
    }

    public function switch(Request $request){

        $category   = Category::findOrFail($request->id);
        $category->status = $request->status=='true' ? 1 : 0;
        $category->update();
    }


    public function rank(Request $request){

     foreach ($request->get('ord') as $key => $value) {

       Category::where(['id' => $value])->update(['rank' => $key]);
   }
}

public function archive(){

    $categories = Category::onlyTrashed()->orderBy('rank', 'ASC')->get();
    return view('dashboard.categories.archive',compact('categories'));
}

public function restore($id){

    $category=Category::onlyTrashed()->findOrFail($id);
    $category->restore();
    return redirect()->route('category.archive')->withNotify('Category restored.');
}

public function harddelete($id){

 Category::onlyTrashed()->findOrFail($id)->forceDelete();
 return redirect()->route('category.archive')->withNotify('Category deleted.');
}

}
