<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'name',
		'description',
		'category',
		'slug',
		'status',
	];

	public function getCategory(){

		return $this->HasOne('App\Category', 'id', 'category');
	}

	
}
