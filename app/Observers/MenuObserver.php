<?php

namespace App\Observers;

use App\Menu;

use Illuminate\Support\Str;

class MenuObserver
{
    /**
     * Handle the Menu "created" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function creating(Menu $menu)
    {
        $menu->slug = Str::slug($menu->name);
    }

    /**
     * Handle the menu "updated" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function updating(Menu $menu)
    {
        $menu->slug = Str::slug($menu->name);
    }

    /**
     * Handle the menu "deleted" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function deleted(Menu $menu)
    {
        //
    }

    /**
     * Handle the menu "restored" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function restored(Menu $menu)
    {
        //
    }

    /**
     * Handle the menu "force deleted" event.
     *
     * @param  \App\menu  $menu
     * @return void
     */
    public function forceDeleted(menu $menu)
    {
        //
    }
}
