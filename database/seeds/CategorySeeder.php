<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$categories = ['Eticaret', 'Kurumsal', 'Emlak', 'Rent A Car', 'Otel&Salon&Pansiyon', 'Restorant', 'One Page'];

    	$descripton = ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque massa erat, venenatis ac mi vel,';

    	$say = 0;
    	foreach ($categories as $category) {$say++;
    		DB::table('categories')->insert([
    			'name'       => $category,
    			'description' => $category.$descripton,
    			'slug'       => Str::slug($category),
    			'rank'       => $say,
    			'created_at' => now(),
    			'updated_at' => now()

    		]);
    	}


    }
}
