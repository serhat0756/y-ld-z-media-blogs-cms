<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$menus = ['Hakkımızda', 'Kariyer','insan Kaynakları', 'İletişim'];

    	$descripton = ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque massa erat, venenatis ac mi vel.';

    	$say = 0;
    	foreach ($menus as $menu) { $say++;
    		DB::table('menus')->insert([
    			'name'       => $menu,
    			'description' => $menu.$descripton,
    			'slug'       => Str::slug($menu),
    			'rank'       => $say,
    			'created_at' => now(),
    			'updated_at' => now()

    		]);
    	}


    }
}
