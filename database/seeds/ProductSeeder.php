<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$products = ['Sigortacı Web Paketi','İnşaat Web Paketi','Temizlik Web Paketi', 'Spor Salonu Web Paketi','Güvenlik Firması Web Paketi', 'Nakliyat Web Paketi', 'Galeri Web Paketi', 'Ürün Tanıtım Web Paketi'];

    	foreach ($products as $key => $product) {
    		DB::table('products')->insert([
    			'name'         => $product,
    			'description'  => $product,
    			'category'     => rand(1,7),
    			'slug'         => Str::slug($product),
    			'created_at'   => now(),
    			'updated_at'   => now()
    		]);
    	}


    }
}
