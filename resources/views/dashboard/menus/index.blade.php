@extends('dashboard.layouts.master')
@section('page', 'Menüler')
@section('content')


<div class="card rounded-0">
    <div class="card-header d-flex align-items-center">
        <h3 class="card-title">{{'Tabloda '.count($menus).' menü bulundu'}}
        </h3>
        <div class="clearfix"></div>
        <hr>
        <a href="{{route('menu.create')}}" class="btn btn-primary rounded-0 mr-2"><i class="fas fa-plus mr-1"></i>Create</a>
        <a href="{{route('menu.archive')}}" class="btn btn-danger rounded-0"><i class="fas fa-file-archive mr-1"></i>Archives</a>

    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <table id="example1" class="table  table-hover">
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Status</th>
                    <th>Actions</th>
                    
                </tr>
            </thead>

            <tbody id="sortable">

                @if(count($menus)>0)

                @foreach($menus as $menu)

                <tr id="ord-{{$menu->id}}">
                    <td style="cursor:move" class="handle"><i class="fas fa-arrows-alt-v"></i></td>
                    <td>{{$menu->name}}</td>
                    <td>{{$menu->created_at->diffForHumans()}}</td>
                    <td>{{$menu->updated_at->diffForHumans()}}</td>

                    <td>
                        <input class="switch" id="{{$menu->id}}" type="checkbox" <?php echo $menu->status==1 ? 'checked' : ''  ?>  data-toggle="toggle" data-on="Aktif" data-off="Pasif" data-onstyle="success" data-offstyle="danger" data-size="sm">
                    </td>


                    <td class="d-flex align-items-center d-md-block">

                       <a href="{{route('menu.show',$menu->id)}}" class="btn btn-sm btn-warning text-white"><i class="fas fa-eye"></i>
                       </a>

                       <a  href="{{route('menu.edit',$menu->id)}}"  class="btn btn-sm btn-success"><i class="fas fa-edit"></i>

                       </a>

                       <form method="POST" action="{{route('menu.destroy', $menu->id) }}" class="d-inline-block"> @csrf                                     
                        @method('DELETE')                                    
                        <button type="submit" class="btn btn-sm btn-danger"> <i class="fas fa-trash-alt"></i>
                        </button> 
                    </form>

                </td>
            </tr>

            @endforeach

            @else

            @endif

        </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
@endsection
@push('css')
<link rel="stylesheet" href="/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush
@push('js')
<script src="/plugins/datatables/jquery.dataTables.js"></script>
<script src="/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });  

    });

    $(function(){
     $('.switch').change(function() {
        var $id     = $(this).attr('id');
        var status = $(this).prop('checked');

        $.get("{{route('menu.switch')}}",{status:status,id:$id}, function(response){});

    });

     $('#sortable').sortable({
        handle: '.handle',
        animation: 150,
        update:function(){
            var rank = $('#sortable').sortable('serialize');

            $.get('{{route("menu.rank")}}?'+rank, function(response){
                
               $('.card-header').after('<div id="ordersuccess" class="alert alert-success rounded-0 py-1">Sıralama Güncellendi</div>');

               setTimeout(function(){
                $("#ordersuccess").fadeOut();
            },2000);
           });
        }
    });
 });

</script>
@endpush
