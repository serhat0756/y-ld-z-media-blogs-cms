@extends('dashboard.layouts.master')
@section('page', 'Kullanıcılar')
@section('content')

<div class="card rounded-0">
    <div class="card-header d-flex align-items-center">
        <h3 class="card-title">{{'Tabloda '.count($users).' kullanıcı bulundu'}}
        </h3>
        <div class="clearfix"></div>
        <hr>
        <a href="{{route('users.create')}}" class="btn btn-primary rounded-0 mr-2"><i class="fas fa-plus mr-1"></i>Create</a>

        <a href="{{route('users.archive')}}" class="btn btn-danger rounded-0"><i class="fas fa-file-archive mr-1"></i>Archives</a>

    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <table id="example1" class="table  table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                @php
                $i=1;
                @endphp

                @foreach($users as $user)
                <tr>

                    <td>{{$i++}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>

                    <td>{{$user->created_at->diffForHumans()}}</td>
                    <td>{{$user->updated_at->diffForHumans()}}</td>

                    <td>
                        <input class="switch" id="{{$user->id}}" type="checkbox" <?php echo $user->status==1 ? 'checked' : ''  ?>  data-toggle="toggle" data-on="Aktif" data-off="Pasif" data-onstyle="success" data-offstyle="danger" data-size="sm">
                    </td>

                    <td class="d-flex align-items-center d-md-block">
                     

                      <a href="{{route('users.show',$user->id)}}" class="btn btn-sm btn-warning text-white"><i class="fas fa-eye"></i>
                      </a>
                      <a  href="{{route('users.edit',$user->id)}}"  class="btn btn-sm btn-success"><i class="fas fa-edit"></i> 
                      </a>

                      <form method="POST" action="{{route('users.destroy', $user->id) }}" class="d-inline-block"> @csrf                                     
                        @method('DELETE')                                    
                        <button type="submit" class="btn btn-sm btn-danger"> <i class="fas fa-trash-alt"></i>
                        </button> </form>
                       

                    </td>
                </tr>

                @endforeach


            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection
@push('css')
<link rel="stylesheet" href="/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endpush
@push('js')
<script src="/plugins/datatables/jquery.dataTables.js"></script>
<script src="/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

    $(function(){
     $('.switch').change(function() {
        var $id     = $(this).attr('id');
        var status = $(this).prop('checked');

        $.get("{{route('users.switch')}}",{status:status,id:$id}, function(response){});

    });
 });
</script>
@endpush
