<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{@asset('/plugins')}}/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{@asset('/dist')}}/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <!-- summernote -->
    <link rel="stylesheet" href="{{@asset('plugins')}}/summernote/summernote-bs4.css">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="{{@asset('/css')}}/pnotify.css">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @stack('css')
</head>
<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        @include('dashboard.layouts.partials.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('dashboard.layouts.partials.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1> @yield('page') </h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">

                                @foreach(request()->segments() as $key => $segment)

                                @if($key==count(request()->segments())-1)

                                <li class="breadcrumb-item">{{ucfirst($segment)}}</li>

                                @else

                                <li class="breadcrumb-item"><a href="#">{{ucfirst($segment)}}</a></li>

                                @endif


                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        @include('dashboard.layouts.partials.footer')

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{@asset('/plugins')}}/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{@asset('/plugins')}}/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{@asset('/dist')}}/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->

    <script src="{{@asset('plugins')}}/summernote/summernote-bs4.min.js"></script>

    <script src="{{@asset('/dist')}}/js/demo.js"></script>
    <script src="{{@asset('/js')}}/pnotify.js"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".breadcrumb").find(".breadcrumb-item:last").addClass("active");

            $(".user_delete").click(function(){

                var route = $(this).data('route');
                var method = $(this).data('method');
            });
        });

    </script>

    <script type="text/javascript">
        $(function () {
    // Summernote
    $('.editor').summernote({
        minHeight: 200,
        focus: true          

    })
})
</script>


@if($errors->any())
<script type="text/javascript">
    new PNotify({
        title: 'Hata!',
        text: '{{$errors->first()}}',
        type: 'error',
    });
</script>
@endif
@if(session('notify'))
<script type="text/javascript">
    new PNotify({
        title: 'Bilgi!',
        text: '{{session('notify')}}',
        type: 'success'
    });


</script>
@endif


@stack('js')
</body>
</html>
