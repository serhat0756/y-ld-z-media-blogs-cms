@extends('dashboard.layouts.master')
@section('page', 'Product Create')
@section('content')
<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title">Horizontal Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{route('product.store')}}" class="form-horizontal" method="POST">
        @csrf

        <div class="card-body">

            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Title</label>
                <div class="col-sm-10">
                    <input name="name" type="text" class="form-control" id="name" placeholder="Title"
                    value="{{old('name')}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="category" class="col-sm-2 col-form-label">Category</label>
                <div class="col-sm-10">
                   <select class="form-control" name="category" id="category">
                    <option value="0">Select a Category</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <textarea  class="form-control editor" name="description" id="description" rows="8">Description</textarea>
            </div>
        </div>

    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button type="submit" class="btn btn-info">Create</button>
        <a href="{{route('product.index')}}" type="button" class="btn btn-default float-right">Cancel</a>
    </div>
    <!-- /.card-footer -->
</form>
</div>

@endsection
