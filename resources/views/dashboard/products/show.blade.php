@extends('dashboard.layouts.master')
@section('content')
<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title">Horizontal Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
   
        <div class="card-body">

            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input name="name" type="text" class="form-control" id="name"
                    value="{{$category->name}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="parent" class="col-sm-2 col-form-label">Parent</label>
                <div class="col-sm-10">
                 <select class="form-control" name="parent" id="parent">
                    <option  <?php echo $category->parent==0 ? 'selected' : ''?> value="0">Üst Kategori</option>
                    @foreach($categories as $allcategory)
                    <option <?php echo  $category->parent==$allcategory->id ? 'selected' : ''?> value="{{$allcategory->id}}" >{{$allcategory->name}}</option>
                    @endforeach

                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <textarea name="description" type="text" class="form-control" id="description"
                value="">{{$category->description}}</textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="parent" class="col-sm-2 col-form-label">Durum</label>
            <div class="col-sm-10">
             <select class="form-control" name="status" id="status">
                <option value="0" <?php echo $category->status==0 ? 'selected' : ''?> >Pasif</option>
                <option value="1" <?php echo $category->status==1 ? 'selected' : ''?> >Aktif</option>

            </select>
        </div>
    </div>

</div>
<!-- /.card-body -->
<div class="card-footer">
    <button type="submit" class="btn btn-info">Create</button>
    <a href="{{route('category.index')}}" type="button" class="btn btn-default float-right">Cancel</a>
</div>
<!-- /.card-footer -->
</form>
</div>

@endsection
