@extends('dashboard.layouts.master')
@section('page', 'Silinmiş Kategoriler')
@section('content')


<div class="card rounded-0">
    <div class="card-header d-flex align-items-center">
        <h3 class="card-title">{{'Tabloda '.count($categories).' kategori bulundu'}}
        </h3>
        <div class="clearfix"></div>
        <hr>
        <a href="{{route('category.index')}}" class="btn btn-info rounded-0 mr-2"><i class="fas fa-eye mr-1"></i>Tüm Kategoriler</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <table id="example1" class="table  table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Created At</th>
                    <th>Deleted At</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>

                @if(count($categories)>0)
                @php $i=1; @endphp

                @foreach($categories as $category)

                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$category->name}}</td>
                    <td>{{$category->created_at->diffForHumans()}}</td>
                    <td>{{$category->updated_at->diffForHumans()}}</td>

                    <td>
                     <span class="text-danger">Deleted</span>
                 </td>


                 <td>
                    <a title="geri yükle" href="{{route('category.restore',$category->id)}}" class="btn btn-sm btn-success "><i class="fas fa-recycle"></i>
                    </a>

                    <a title="sil" href="{{route('category.hard.delete',$category->id)}}" class="btn btn-sm btn-danger deletebtn "><i class="fas fa-trash-alt"></i>
                    </a>

                </td>
            </tr>

            @endforeach

            @else

            @endif

        </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
@endsection
@push('css')
<link rel="stylesheet" href="{{@asset('/plugins')}}/datatables-bs4/css/dataTables.bootstrap4.css">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{@asset('/plugins')}}/sweetalert2/sweetalert2.min.css">

@endpush
@push('js')
<script src="{{@asset('/plugins')}}/datatables/jquery.dataTables.js"></script>
<script src="{{@asset('/plugins')}}/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="{{@asset('/plugins')}}/sweetalert2/sweetalert2.min.js"></script>

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });  

    });

    $(function(){
     $('.switch').change(function() {
        var $id     = $(this).attr('id');
        var status = $(this).prop('checked');

        $.get("{{route('category.switch')}}",{status:status,id:$id}, function(response){});

    });



 });

    $(".deletebtn").on("click", function(event){

     event.preventDefault();

     var data_url = $(this).attr("href");

     Swal.fire({
        title: 'Gerçekten Silmek İstiyormusunuz ? ',
        text: "Bu işlemi geri alamazsınız!",
        type: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet, Sil',
        cancelButtonText :  'Hayır'     
    }).then((result) => {
        if (result.value) {
            window.location.href = data_url;
        }
    })
});

</script>
@endpush
