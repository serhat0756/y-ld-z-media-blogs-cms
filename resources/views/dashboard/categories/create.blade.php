@extends('dashboard.layouts.master')
@section('content')
<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title">Horizontal Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{route('category.store')}}" class="form-horizontal" method="POST">
        @csrf

        <div class="card-body">

            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input name="name" type="text" class="form-control" id="name" placeholder="Name"
                    value="{{old('name')}}">
                </div>
            </div>
<div class="form-group row">
                <label for="parent" class="col-sm-2 col-form-label">Parent</label>
                <div class="col-sm-10">
                   <select class="form-control" name="parent" id="parent">
                    <option value="0">Üst Kategori Seçiniz</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
            

        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="4" name="description" id="description" rows="4"></textarea>
            </div>
        </div>

    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button type="submit" class="btn btn-info">Create</button>
       <a href="{{route('category.index')}}" type="button" class="btn btn-default float-right">Cancel</a>
    </div>
    <!-- /.card-footer -->
</form>
</div>

@endsection
